package com.ajuenterprises89.snapstore.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import java.text.SimpleDateFormat

object Utils {
    val getclick = SingleLiveEvent<Int>()


    val emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\\.+[a-z]+"
    fun Fragment.hideKeyboard() {
        view?.let { activity?.hideKeyboard(it) }
    }

    fun Activity.hideKeyboard() {
        hideKeyboard(currentFocus ?: View(this))
    }

    fun Context.hideKeyboard(view: View) {
        val inputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun isEmailValid(email: String): Boolean {
        return emailPattern.toRegex().matches(email)
    }




    fun checkAndRequestPermissions(
        activity: Activity,
        permission: String,
        requestCode: Int
    ): Boolean {
        val listPermissionsNeeded = arrayListOf<String>()
        if (permission == Manifest.permission.CAMERA) {
            if (ContextCompat.checkSelfPermission(
                    activity,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) !== PackageManager.PERMISSION_GRANTED
            ) {
                listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE)
            }
            if (ContextCompat.checkSelfPermission(
                    activity,
                    Manifest.permission.CAMERA
                ) !== PackageManager.PERMISSION_GRANTED
            ) {
                listPermissionsNeeded.add(Manifest.permission.CAMERA)
            }
        } else if (permission == Manifest.permission.WRITE_CALENDAR) {
            if (ContextCompat.checkSelfPermission(
                    activity,
                    Manifest.permission.WRITE_CALENDAR
                ) !== PackageManager.PERMISSION_GRANTED
            ) {
                listPermissionsNeeded.add(Manifest.permission.WRITE_CALENDAR)
            }
            if (ContextCompat.checkSelfPermission(
                    activity,
                    Manifest.permission.READ_CALENDAR
                ) !== PackageManager.PERMISSION_GRANTED
            ) {
                listPermissionsNeeded.add(Manifest.permission.READ_CALENDAR)
            }
        } else {
            if (ContextCompat.checkSelfPermission(
                    activity,
                    permission
                ) !== PackageManager.PERMISSION_GRANTED
            ) {
                listPermissionsNeeded.add(permission)
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            requestPermissions(
                activity,
                listPermissionsNeeded.toTypedArray(),
                requestCode
            )

            return false
        }
        return true
    }




    fun convertDate(date:String,fromDate:String,toDate: String?):String{
        var newDate = ""
        try {
            var spf = SimpleDateFormat(fromDate)
            var date = spf.parse(date)
            var newSpf = SimpleDateFormat(toDate)
            newDate = newSpf.format(date)
        } catch (e: Exception) {
        }
        return newDate
    }

}