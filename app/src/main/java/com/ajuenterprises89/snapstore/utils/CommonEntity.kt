package com.ajuenterprises89.snapstore.utils

import androidx.annotation.Keep

@Keep
data class CommonEntity (
    val ApiToken: Any,
    val CompanyName: String,
    val IsAuthourized: String,
    val Message: String,
    val TimeStamp: String,
    val TransactionStatus: String
)