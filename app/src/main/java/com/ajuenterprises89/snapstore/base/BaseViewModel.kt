package com.ajuenterprises89.snapstore.base


import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class BaseViewModel<T> : ViewModel() {
    val loadingStatus: MutableLiveData<Boolean> = MutableLiveData()
}