package com.ajuenterprises89.snapstore.base

import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar

open class BaseActivity : AppCompatActivity() {

    fun showToast(msg: String) {
        Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
    }

    fun showLongToast(msg: String) {
        Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
    }

    fun showSnakbar(msg: String, view: View) {
        val snack = Snackbar.make(view, msg, Snackbar.LENGTH_LONG)
        snack.show()
    }

}