package com.ajuenterprises89.snapstore.base

import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar

open class BaseFragment : Fragment() {

    fun showToast(msg: String) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
    }

    fun showLongToast(msg: String) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
    }

    fun showSnakbar(msg: String, view: View) {
        val snack = Snackbar.make(view, msg, Snackbar.LENGTH_LONG)
        snack.show()
    }
}