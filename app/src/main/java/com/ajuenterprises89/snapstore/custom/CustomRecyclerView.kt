package com.issacbs_shipmanagement.rio.seafarerportalandroid.custom

import android.content.Context
import android.util.AttributeSet
import android.view.inputmethod.InputMethodManager
import androidx.annotation.Nullable
import androidx.recyclerview.widget.RecyclerView

class CustomRecyclerView @JvmOverloads constructor(
    context: Context, @Nullable attrs: AttributeSet? = null,
    defStyle: Int = -1
) : RecyclerView(context, attrs, defStyle) {
    private var onKeyboardDismissingScrollListener: RecyclerView.OnScrollListener? = null
    private var inputMethodManager: InputMethodManager? = null

    init {
        setOnKeyboardDismissingListener()
    }

    /**
     * Creates [RecyclerView.OnScrollListener] that will dismiss keyboard when scrolling if the keyboard
     * has not been dismissed internally before
     */
    private fun setOnKeyboardDismissingListener() {
        onKeyboardDismissingScrollListener = object : RecyclerView.OnScrollListener() {
            internal var isKeyboardDismissedByScroll: Boolean = false

            override fun onScrollStateChanged(recyclerView: RecyclerView, state: Int) {
                when (state) {
                    SCROLL_STATE_DRAGGING -> if (!isKeyboardDismissedByScroll) {
                        hideKeyboard()
                        isKeyboardDismissedByScroll = !isKeyboardDismissedByScroll
                    }
                    SCROLL_STATE_IDLE -> isKeyboardDismissedByScroll = false
                }
            }
        }
    }

    protected override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        addOnScrollListener(onKeyboardDismissingScrollListener!!)
    }

    protected override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        removeOnScrollListener(onKeyboardDismissingScrollListener!!)
    }

    /**
     * Hides the keyboard
     */
    fun hideKeyboard() {
        getInputMethodManager()!!.hideSoftInputFromWindow(getWindowToken(), 0)
        clearFocus()
    }

    /**
     * Returns an [InputMethodManager]
     *
     * @return input method manager
     */
    fun getInputMethodManager(): InputMethodManager? {
        if (null == inputMethodManager) {
            inputMethodManager = getContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        }

        return inputMethodManager
    }
}
