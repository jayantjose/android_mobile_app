package com.ajuenterprises89.snapstore.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

import com.ajuenterprises89.snapstore.R;


public class CustomTextViewBold extends AppCompatTextView {

    String defaultFont = "SF-UI-Display-Bold.otf";

    public CustomTextViewBold(Context context) {
        super(context);
        initAttributes(null);
    }

    public CustomTextViewBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttributes(attrs);
    }

    public CustomTextViewBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttributes(attrs);
    }

    public void initAttributes(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CustomTextView);
            String fontName = a.getString(R.styleable.CustomTextView_fontName);
            try {
                if (fontName != null) {
                    Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + defaultFont);
                    setTypeface(myTypeface);
                } else {
                    Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + defaultFont);
                    setTypeface(myTypeface);
                }
            } catch (Exception e) {
                e.printStackTrace();

            }

            a.recycle();

        }

    }

}
