package com.ajuenterprises89.snapstore.ui.login.view.activity

import android.os.Bundle
import com.ajuenterprises89.snapstore.R
import com.ajuenterprises89.snapstore.base.BaseActivity
import com.ajuenterprises89.snapstore.ui.login.view.fragment.SignInFragment
import com.ajuenterprises89.snapstore.utils.FragmentUtils

class LoginActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initView()
    }

    private fun initView() {
        FragmentUtils.replaceFragment(
            this,
            SignInFragment(),
            R.id.fragmentContainer,
            false
        )
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }
}