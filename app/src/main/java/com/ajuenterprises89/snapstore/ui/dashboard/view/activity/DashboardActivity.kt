package com.ajuenterprises89.snapstore.ui.dashboard.view.activity

import android.os.Bundle
import com.ajuenterprises89.snapstore.R
import com.ajuenterprises89.snapstore.base.BaseActivity
import com.ajuenterprises89.snapstore.ui.cart.CartFragment
import com.ajuenterprises89.snapstore.ui.dashboard.view.fragment.DashboardFragment
import com.ajuenterprises89.snapstore.ui.favorites.view.FavlListFragment
import com.ajuenterprises89.snapstore.ui.profile.view.ProfileFragment
import com.ajuenterprises89.snapstore.ui.settings.view.SettingsFragment
import com.ajuenterprises89.snapstore.utils.FragmentUtils
import com.etebarian.meowbottomnavigation.MeowBottomNavigation
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        initView()
        loadBottomBar()

    }


    private fun initView() {
        FragmentUtils.addFragment(
            this,
            DashboardFragment(),
            R.id.dashboardContainer,
            false
        )
    }


    private fun loadBottomBar() {
        bottomNavigation.add(
            MeowBottomNavigation.Model(
                1,
                R.drawable.ic_home
            )
        )
        bottomNavigation.add(
            MeowBottomNavigation.Model(
                2,
                R.drawable.ic_heart
            )
        )
        bottomNavigation.add(
            MeowBottomNavigation.Model(
                3,
                R.drawable.ic_carticon
            )
        )
        bottomNavigation.add(
            MeowBottomNavigation.Model(
                4,
                R.drawable.ic_settings
            )
        )

        bottomNavigation.add(
            MeowBottomNavigation.Model(
                5,
                R.drawable.ic_profile
            )
        )

        bottomNavigation.setOnShowListener {
            when (it.id) {
                1->{
                    FragmentUtils.replaceFragment(
                        this,
                        DashboardFragment(),
                        R.id.dashboardContainer,
                        false
                    )
                }
                2->{
                    FragmentUtils.replaceFragment(
                        this,
                        FavlListFragment(),
                        R.id.dashboardContainer,
                        false
                    )
                }
                3->{
                    FragmentUtils.replaceFragment(
                        this,
                        CartFragment(),
                        R.id.dashboardContainer,
                        false
                    )
                }
                4 -> {
                    FragmentUtils.replaceFragment(
                        this,
                        SettingsFragment(),
                        R.id.dashboardContainer,
                        false
                    )

                }

                5 -> {
                    FragmentUtils.replaceFragment(
                        this,
                        ProfileFragment(),
                        R.id.dashboardContainer,
                        false
                    )

                }
            }
        }
    }

}