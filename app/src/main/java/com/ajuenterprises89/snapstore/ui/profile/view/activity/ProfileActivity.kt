package com.ajuenterprises89.snapstore.ui.profile.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ajuenterprises89.snapstore.R
import com.ajuenterprises89.snapstore.ui.dashboard.view.fragment.DashboardFragment
import com.ajuenterprises89.snapstore.ui.profile.view.ProfileFragment
import com.ajuenterprises89.snapstore.utils.FragmentUtils

class ProfileActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        initView()
    }
    private fun initView() {
        FragmentUtils.addFragment(
            this,
            ProfileFragment(),
            R.id.profileContainer,
            false
        )
    }

}