package com.ajuenterprises89.snapstore.ui.product.view.fragment

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.ajuenterprises89.snapstore.R
import com.ajuenterprises89.snapstore.databinding.FragmentCategoryBinding
import com.ajuenterprises89.snapstore.databinding.ProductListFragmentBinding
import com.ajuenterprises89.snapstore.ui.product.view.adapter.CompleteProductListingAdapter
import com.ajuenterprises89.snapstore.ui.product.viewmodel.ProductListViewModel
import kotlinx.android.synthetic.main.dashboard_main.*
import kotlinx.android.synthetic.main.product_list_fragment.*
import kotlinx.android.synthetic.main.toolbar.view.*

class ProductListFragment : Fragment() {

    lateinit var mBinding: ProductListFragmentBinding
    private lateinit var viewModel: ProductListViewModel
    lateinit var pdtAdapter:CompleteProductListingAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
       // return inflater.inflate(R.layout.product_list_fragment, container, false)
        mBinding = DataBindingUtil.inflate(inflater, R.layout.product_list_fragment, container, false)
        mBinding.lifecycleOwner = viewLifecycleOwner
        return mBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ProductListViewModel::class.java)
        mBinding.viewModel = viewModel
        initView()

        loveDataObserver()
    }

    private fun loveDataObserver() {
     viewModel.mProductList.observe(activity!!, Observer {
         if (it.success.data.products.isNotEmpty()) {
             pdtAdapter =
                 CompleteProductListingAdapter(it.success.data.products, context!!,it.success.data.imagePath)
             rvproducts.adapter = pdtAdapter

         }
     })
    }
    private fun initView() {
        mBinding.toolbar.title_text.text = "Product List"
        viewModel.loadProducList()
    }

}