package com.ajuenterprises89.snapstore.ui.favorites.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.ajuenterprises89.snapstore.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.product_item_view_all.view.*

class WhilistRecyclerGridAdapter (val context: Context) : BaseAdapter() {
    private var layoutInflater: LayoutInflater? = null

    override fun getItem(position: Int): Any {
        // return mCategoryList.get(position)
        return 0
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return 10
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var convertView = convertView
        if (layoutInflater == null) {
            layoutInflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }
        if (convertView == null) {
            convertView = layoutInflater!!.inflate(R.layout.whistle_gridcard, null)
        }

        return convertView!!
    }

}