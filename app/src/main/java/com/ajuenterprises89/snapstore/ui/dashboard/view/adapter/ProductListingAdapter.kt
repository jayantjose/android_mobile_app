package com.ajuenterprises89.snapstore.ui.dashboard.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ajuenterprises89.snapstore.R
import com.ajuenterprises89.snapstore.base.Constants.PRODUCT_URL
import com.ajuenterprises89.snapstore.ui.dashboard.model.Product
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.card_imagelist.view.*
import kotlinx.android.synthetic.main.card_imagelist.view.iv_imageSlider
import kotlinx.android.synthetic.main.product_item_view.view.*

class ProductListingAdapter(
    val data: List<Product>,
    val context: Context?
) :
    RecyclerView.Adapter<ProductListingAdapter.ViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ProductListingAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.product_item_view, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return 5
    }

    override fun onBindViewHolder(holder: ProductListingAdapter.ViewHolder, position: Int) {
        val data=data[position]

        Picasso.with(context).load(PRODUCT_URL+data.photo)
            .into(holder.itemView.iv_imageSlider)
        holder.itemView.tvAmount.text="$ "+data.price.toString()
        holder.itemView.tvProductName.text=data.name.toString()

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

}