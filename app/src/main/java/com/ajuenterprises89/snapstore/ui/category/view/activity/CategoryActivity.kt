package com.ajuenterprises89.snapstore.ui.category.view.activity

import android.os.Bundle
import com.ajuenterprises89.snapstore.R
import com.ajuenterprises89.snapstore.base.BaseActivity
import com.ajuenterprises89.snapstore.ui.category.view.fragment.CategoryFragment
import com.ajuenterprises89.snapstore.utils.FragmentUtils

class CategoryActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_category)
        initView()
    }

    private fun initView() {
        val fragment = CategoryFragment()
        var bundle = Bundle()
        bundle.putSerializable("data",  intent.getSerializableExtra("category"))
        fragment.arguments = bundle
        FragmentUtils.addFragment(
            this,
            fragment,
            R.id.categoryContainer,
            false
        )
    }
}
