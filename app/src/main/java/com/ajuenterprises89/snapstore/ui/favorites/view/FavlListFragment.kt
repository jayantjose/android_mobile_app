package com.ajuenterprises89.snapstore.ui.favorites.view

import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.ajuenterprises89.snapstore.R
import com.ajuenterprises89.snapstore.databinding.FavlListFragmentBinding
import com.ajuenterprises89.snapstore.ui.dashboard.view.activity.DashboardActivity
import com.ajuenterprises89.snapstore.ui.dashboard.view.adapter.NewArrivalAdapter
import com.ajuenterprises89.snapstore.ui.dashboard.view.fragment.DashboardFragment
import com.ajuenterprises89.snapstore.ui.favorites.adapters.WhilistRecyclerAdapter
import com.ajuenterprises89.snapstore.ui.favorites.adapters.WhilistRecyclerGridAdapter
import com.ajuenterprises89.snapstore.ui.favorites.viewmodel.FavlListViewModel
import com.ajuenterprises89.snapstore.ui.product.view.adapter.CompleteProductListingAdapter
import com.ajuenterprises89.snapstore.utils.FragmentUtils
import kotlinx.android.synthetic.main.common_toolbar.*
import kotlinx.android.synthetic.main.dashboard_main.*
import kotlinx.android.synthetic.main.favl_list_fragment.*
import kotlinx.android.synthetic.main.product_list_fragment.*
import kotlinx.android.synthetic.main.toolbar.*

class FavlListFragment : Fragment(),View.OnClickListener {

    lateinit var mBinding: FavlListFragmentBinding
    private lateinit var viewModel: FavlListViewModel
    lateinit var gdADapter:WhilistRecyclerGridAdapter
    lateinit var mFavAdapter:WhilistRecyclerAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //return inflater.inflate(R.layout.favl_list_fragment, container, false)
        mBinding = DataBindingUtil.inflate(inflater, R.layout.favl_list_fragment, container, false)
        mBinding.lifecycleOwner = viewLifecycleOwner
        return mBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(FavlListViewModel::class.java)

        toolbarClick()
        onClickEvents()
        loadGridDate()
        loadRecyclerData()
    }

    private fun toolbarClick() {
        tvName.text="WISHLIST"
        ivDrawerBack.setOnClickListener {
            startActivity(Intent(context, DashboardActivity::class.java))
            activity!!.finish()
        }
    }

    private fun onClickEvents() {
        imgIconTypeNormal.setOnClickListener(this)
        imgIconTypeGrid.setOnClickListener(this)
    }

    private fun loadRecyclerData() {
        mFavAdapter = WhilistRecyclerAdapter(context)
        rvFavlist.adapter = mFavAdapter
    }

    private fun loadGridDate() {
        gdADapter=WhilistRecyclerGridAdapter(context!!)
        gdFavList.adapter = gdADapter
    }

    override fun onClick(p0: View?) {
        when(p0!!.id){
            R.id.imgIconTypeNormal->{
                rvFavlist.visibility=View.VISIBLE
                gdFavList.visibility=View.GONE
                imgIconTypeNormal.setBackgroundResource(R.drawable.rectangle_black)
                imgIconTypeGrid.setBackgroundResource(R.drawable.rectangle_white)
                ivNormal.setImageResource(R.drawable.ic_normalview_white)
                ivGrid.setImageResource(R.drawable.ic_grid_black)

            }
            R.id.imgIconTypeGrid->{
                ivNormal.setImageResource(R.drawable.ic_normalview_black)
                ivGrid.setImageResource(R.drawable.ic_grid_white)
                imgIconTypeNormal.setBackgroundResource(R.drawable.rectangle_white)
                imgIconTypeGrid.setBackgroundResource(R.drawable.rectangle_black)
                rvFavlist.visibility=View.GONE
                gdFavList.visibility=View.VISIBLE
            }
        }
    }

}