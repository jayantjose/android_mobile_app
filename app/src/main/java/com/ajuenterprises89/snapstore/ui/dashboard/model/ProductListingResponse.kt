package com.ajuenterprises89.snapstore.ui.dashboard.model

data class ProductListingResponse(
    val isSuccess: Boolean,
    val success: SuccessPdt
)

data class SuccessPdt(
    val `data`: DataPdt,
    val message: String
)

data class DataPdt(
    val imagePath: String,
    val products: List<Product>
)

data class Product(
    val attributes: Any,
    val colors: String,
    val discount_date: Any,
    val features: String,
    val id: Int,
    val name: String,
    val photo: String,
    val previous_price: Int,
    val price: Int,
    val size: String,
    val size_price: String,
    val slug: String,
    val thumbnail: String,
    val user: User,
    val user_id: Int
)

data class User(
    val address: String,
    val affilate_code: String,
    val affilate_income: Int,
    val ban: Int,
    val city: String,
    val country: String,
    val created_at: String,
    val current_balance: Double,
    val date: String,
    val email: String,
    val email_verified: String,
    val f_check: Int,
    val f_url: Any,
    val fax: String,
    val g_check: Int,
    val g_url: Any,
    val id: Int,
    val is_provider: Int,
    val is_vendor: Int,
    val l_check: Int,
    val l_url: Any,
    val mail_sent: Int,
    val name: String,
    val owner_name: String,
    val phone: String,
    val photo: String,
    val reg_number: String,
    val shipping_cost: Int,
    val shop_address: String,
    val shop_details: String,
    val shop_image: Any,
    val shop_message: String,
    val shop_name: String,
    val shop_number: String,
    val status: Int,
    val t_check: Int,
    val t_url: Any,
    val updated_at: String,
    val verification_link: String,
    val zip: String
)