package com.ajuenterprises89.snapstore.ui.favorites.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ajuenterprises89.snapstore.R
import com.ajuenterprises89.snapstore.interfaces.RvItemClicked
import com.ajuenterprises89.snapstore.ui.category.adapter.CategoryListAdapter
import com.ajuenterprises89.snapstore.ui.category.models.CategoryProductResponse
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.category_main_view.view.*

class WhilistRecyclerAdapter (
    val context: Context?
) :
    RecyclerView.Adapter<WhilistRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v =
            LayoutInflater.from(parent.context).inflate(R.layout.whistle_recyclercard, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return 10
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }
}
