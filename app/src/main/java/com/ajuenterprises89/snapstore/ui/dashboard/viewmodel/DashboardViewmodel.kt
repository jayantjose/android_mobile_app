package com.ajuenterprises89.snapstore.ui.dashboard.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.ajuenterprises89.snapstore.base.BaseViewModel
import com.ajuenterprises89.snapstore.network.Repository
import com.ajuenterprises89.snapstore.ui.dashboard.model.*
import com.ajuenterprises89.snapstore.utils.SingleLiveEvent
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.RequestBody

class DashboardViewmodel : BaseViewModel<Any>() {
    var mCategoryList = MutableLiveData<CategoryListResponse>()
    var mProductList = MutableLiveData<ProductListingResponse>()
    var mSubCategoryList = MutableLiveData<SubCategoryResponse>()
    var mBannerList = MutableLiveData<BannerList>()
    var mLatestProductList = MutableLiveData<LatestProductsResponse>()
    val isloading = SingleLiveEvent<Boolean>()

    fun getCategoryList() {
        viewModelScope.launch {
            try {
                isloading.postValue(true)
                val call = Repository.categoryListing()
                if (call != null) {
                    isloading.postValue(false)
                    call.apply {
                        takeIf { isSuccessful } ?: throw Exception("Network Error")
                        takeIf { body() != null } ?: throw Exception("Server Error")
                        mCategoryList.postValue(this.body())
                    }
                } else {
                    isloading.postValue(false)
                    throw Exception("No Internet")
                }
            } catch (e: Exception) {
                isloading.postValue(false)
                Log.e("Exception", e.localizedMessage)
            }
        }

    }

    fun getProductList() {
        viewModelScope.launch {
            try {
                isloading.postValue(true)
                val call = Repository.productListing()
                if (call != null) {
                    isloading.postValue(false)
                    call.apply {
                        takeIf { isSuccessful } ?: throw Exception("Network Error")
                        takeIf { body() != null } ?: throw Exception("Server Error")
                        mProductList.postValue(this.body())
                    }
                } else {
                    isloading.postValue(false)
                    throw Exception("No Internet")
                }
            } catch (e: Exception) {
                isloading.postValue(false)
                Log.e("Exception", e.localizedMessage)
            }
        }

    }

     fun getSubcateoryList() {
        viewModelScope.launch {
            try {
                val call = Repository.subCategoryListing()
                if (call != null) {
                    call.apply {
                        takeIf { isSuccessful } ?: throw Exception("Network Error")
                        takeIf { body() != null } ?: throw Exception("Server Error")
                        mSubCategoryList.postValue(this.body())
                    }
                } else {
                    throw Exception("No Internet")
                }
            } catch (e: Exception) {
                Log.e("Exception", e.localizedMessage)
            }
        }

    }

    fun loadBannerList(){
        viewModelScope.launch {
            try {
                isloading.postValue(true)
                val call = Repository.bannerList()
                if (call != null) {
                    isloading.postValue(false)
                    call.apply {
                        takeIf { isSuccessful } ?: throw Exception("Network Error")
                        takeIf { body() != null } ?: throw Exception("Server Error")
                        mBannerList.postValue(this.body())
                    }
                } else {
                    isloading.postValue(false)
                    throw Exception("No Internet")
                }
            } catch (e: Exception) {
                isloading.postValue(false)
                Log.e("Exception", e.localizedMessage)
            }
        }
    }

    fun latestProducts(){
        viewModelScope.launch {
            try {
                isloading.postValue(true)
                val call = Repository.latestProducts()
                if (call != null) {
                    isloading.postValue(false)
                    call.apply {
                        takeIf { isSuccessful } ?: throw Exception("Network Error")
                        takeIf { body() != null } ?: throw Exception("Server Error")
                        mLatestProductList.postValue(this.body())
                    }
                } else {
                    isloading.postValue(false)
                    throw Exception("No Internet")
                }
            } catch (e: Exception) {
                Log.e("Exception", e.localizedMessage)
            }
        }
    }
}