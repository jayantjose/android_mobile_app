package com.ajuenterprises89.snapstore.ui.dashboard.model

data class BannerList(
    val isSuccess: Boolean,
    val success: BannerSuccess
)

data class BannerSuccess(
    val `data`: BannerData,
    val message: String
)

data class BannerData(
    val imagePath: String,
    val sliders: List<Slider>
)

data class Slider(
    val details_anime: String,
    val details_color: String,
    val details_size: String,
    val details_text: String,
    val details_text_ar: Any,
    val id: Int,
    val link: String,
    val photo: String,
    val position: String,
    val subtitle_anime: String,
    val subtitle_color: String,
    val subtitle_size: String,
    val subtitle_text: String,
    val subtitle_text_ar: Any,
    val title_anime: String,
    val title_color: String,
    val title_size: String,
    val title_text: String,
    val title_text_ar: String
)