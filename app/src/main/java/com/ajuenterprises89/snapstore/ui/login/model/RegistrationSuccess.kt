package com.ajuenterprises89.snapstore.ui.login.model

import com.ajuenterprises89.snapstore.ui.login.model.Data

data class RegistrationSuccess(
    val `data`: SignUpData,
    val message: String,
    val success: String
)

data class SignUpData(
    val userid: Int,
    val username: String
)