package com.ajuenterprises89.snapstore.ui.dashboard.model

import android.graphics.drawable.Drawable

data class NavigationMenuItem(val title:String,val subTitle:String,var icon: Int)
