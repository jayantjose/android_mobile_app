package com.ajuenterprises89.snapstore.ui.payment.view

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.ajuenterprises89.snapstore.R
import com.ajuenterprises89.snapstore.databinding.PaymentFragmentBinding
import com.ajuenterprises89.snapstore.ui.dashboard.model.Slider
import com.ajuenterprises89.snapstore.ui.dashboard.view.activity.DashboardActivity
import com.ajuenterprises89.snapstore.ui.dashboard.view.adapter.BannerListAdapter
import com.ajuenterprises89.snapstore.ui.dashboard.view.adapter.PaymentAdapter
import com.ajuenterprises89.snapstore.ui.payment.viewmodels.PaymentViewModel
import kotlinx.android.synthetic.main.common_toolbar.*
import kotlinx.android.synthetic.main.dashboard_main.*
import kotlinx.android.synthetic.main.payment_fragment.*
import liang.lollipop.rvbannerlib.BannerUtil
import liang.lollipop.rvbannerlib.banner.Orientation
import java.util.*
import javax.xml.datatype.DatatypeConstants.MONTHS

class PaymentFragment : Fragment() ,View.OnClickListener{

    lateinit var  mBinding:PaymentFragmentBinding

    private lateinit var viewModel: PaymentViewModel
    lateinit var bannerListAdapter: PaymentAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.payment_fragment, container, false)
        mBinding.lifecycleOwner = viewLifecycleOwner
        return mBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(PaymentViewModel::class.java)
        initClick()
        loadToolBar()
        loadViewPager()
    }

    private fun initClick() {
        etCardDate.setOnClickListener(this)
    }

    private fun loadViewPager() {
        bannerListAdapter = PaymentAdapter(activity,layoutInflater)
        BannerUtil.with(rvPayments)
            .attachAdapter(bannerListAdapter)
            .setOrientation(Orientation.HORIZONTAL)
            .setSecondaryExposedWeight(0.15F)
            .init()
            .isAutoNext(auto = true)

        rvPayments.adapter!!.notifyDataSetChanged()
    }

    private fun loadToolBar() {
        tvName.text="PAYMENT METHOD"
        ivDrawerBack.setOnClickListener {
            startActivity(Intent(context, DashboardActivity::class.java))
            activity!!.finish()
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onClick(p0: View?) {
        when(p0!!.id){
            R.id.etCardDate->{
                val calendar= Calendar.getInstance()
                val year = calendar.get(Calendar.YEAR)
                val month = calendar.get(Calendar.MONTH)
                val day = calendar.get(Calendar.DAY_OF_MONTH)
                val datePickerDialog = DatePickerDialog(activity!!, DatePickerDialog.OnDateSetListener
                { view, year, monthOfYear, dayOfMonth ->


                }, year, month, day)
                datePickerDialog.show()
                etCardDate.text=day.toString()+ "/"+month+"/"+year

            }
        }
    }

}