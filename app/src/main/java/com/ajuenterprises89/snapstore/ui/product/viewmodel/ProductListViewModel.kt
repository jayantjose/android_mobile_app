package com.ajuenterprises89.snapstore.ui.product.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.ajuenterprises89.snapstore.base.BaseViewModel
import com.ajuenterprises89.snapstore.network.Repository
import com.ajuenterprises89.snapstore.ui.product.models.ProductListResponse
import com.ajuenterprises89.snapstore.utils.SingleLiveEvent
import kotlinx.coroutines.launch

class ProductListViewModel : BaseViewModel<Any>() {
    var mProductList=MutableLiveData<ProductListResponse>()
    val isloading = SingleLiveEvent<Boolean>()
    fun loadProducList(){

        viewModelScope.launch {
            isloading.postValue(true)
            try {

                val call = Repository.productListViewAll()
                if (call != null) {
                    isloading.postValue(false)
                    call.apply {
                        takeIf { isSuccessful } ?: throw Exception("Network Error")
                        takeIf { body() != null } ?: throw Exception("Server Error")
                        mProductList.postValue(this.body())
                    }
                } else {
                    isloading.postValue(false)
                    throw Exception("No Internet")
                }
            } catch (e: Exception) {
                isloading.postValue(false)
                Log.e("Exception", e.localizedMessage)
            }
        }

    }
}