package com.ajuenterprises89.snapstore.ui.login.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ajuenterprises89.snapstore.network.Repository
import com.ajuenterprises89.snapstore.ui.login.model.RegistrationSuccess
import com.ajuenterprises89.snapstore.utils.SingleLiveEvent
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.RequestBody




class RegistrationViewModel : ViewModel() {
    val isloading = SingleLiveEvent<Boolean>()

    var mErrorMessage=MutableLiveData<String>()
    var mRegResponse=MutableLiveData<RegistrationSuccess>()
    fun registration(email: String, password: String) {
            viewModelScope.launch {
                isloading.value=true
                try {
                    val reqUsername: RequestBody = RequestBody.create(MediaType.parse("text/plain"), email)
                    val reqPass=    RequestBody.create(MediaType.parse("text/plain"), password)
                    val reqname=    RequestBody.create(MediaType.parse("text/plain"), "null")
                    val phone=    RequestBody.create(MediaType.parse("text/plain"), "null")
                    val address=    RequestBody.create(MediaType.parse("text/plain"), "null")
                    val password_confirmation=    RequestBody.create(MediaType.parse("text/plain"), password)

                   // val call = Repository.registration(email,password)
                    val call = Repository.registration(reqUsername,reqPass,reqname,phone,address,password_confirmation)
                    if (call != null) {
                        isloading.value=false

                        call.apply {
                            takeIf { isSuccessful } ?: throw Exception("Network Error")
                            takeIf { body() != null } ?: throw Exception("Server Error")
                            if(this.body()!!.success=="true"){
                                mRegResponse.postValue(this.body())
                            }else{
                                mErrorMessage.postValue("The email has already been taken.")
                            }
                        }
                    } else {
                        isloading.value=false

                        throw Exception("No Internet")
                    }
                } catch (e: Exception) {
                    isloading.value=false

                    Log.e("Exception", e.localizedMessage)
                }
            }



    }
}