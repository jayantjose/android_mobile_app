package com.ajuenterprises89.snapstore.ui.profile.view

import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.ajuenterprises89.snapstore.R
import com.ajuenterprises89.snapstore.databinding.FragmentDashboardBinding
import com.ajuenterprises89.snapstore.databinding.ProfileFragmentBinding
import com.ajuenterprises89.snapstore.ui.dashboard.view.activity.DashboardActivity
import com.ajuenterprises89.snapstore.ui.profile.viewmodel.ProfileViewModel
import kotlinx.android.synthetic.main.common_toolbar.*

class ProfileFragment : Fragment() {
    lateinit var mBinding:ProfileFragmentBinding

    private lateinit var viewModel: ProfileViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.profile_fragment, container, false)
        mBinding.lifecycleOwner = viewLifecycleOwner
        return mBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)
        toolbarClick()
    }
    private fun toolbarClick() {
        tvName.text="PROFILE"
        ivDrawerBack.setOnClickListener {
            startActivity(Intent(context, DashboardActivity::class.java))
            activity!!.finish()
        }
    }

}