package com.ajuenterprises89.snapstore.ui.dashboard.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ajuenterprises89.snapstore.R

class SubCategoryListingAdapter : RecyclerView.Adapter<SubCategoryListingAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubCategoryListingAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.card_imagelist, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return 10
    }

    override fun onBindViewHolder(holder: SubCategoryListingAdapter.ViewHolder, position: Int) {
        TODO("Not yet implemented")
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

}