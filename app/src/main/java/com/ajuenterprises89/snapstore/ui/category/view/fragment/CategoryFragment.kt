package com.ajuenterprises89.snapstore.ui.category.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ajuenterprises89.snapstore.R
import com.ajuenterprises89.snapstore.base.BaseFragment
import com.ajuenterprises89.snapstore.databinding.FragmentCategoryBinding
import com.ajuenterprises89.snapstore.interfaces.RvItemClicked
import com.ajuenterprises89.snapstore.ui.category.adapter.CategoryListAdapter
import com.ajuenterprises89.snapstore.ui.category.models.CategoryProductResponse
import com.ajuenterprises89.snapstore.ui.category.viewmodel.CategoryViewmodel
import com.ajuenterprises89.snapstore.ui.dashboard.model.CategoryLists
import com.ajuenterprises89.snapstore.utils.FragmentUtils
import kotlinx.android.synthetic.main.fragment_category.*
import kotlinx.android.synthetic.main.toolbar.view.*

class CategoryFragment : BaseFragment(), RvItemClicked {

    lateinit var mBinding: FragmentCategoryBinding
    lateinit var mViewmodel: CategoryViewmodel
    lateinit var mCategory: CategoryLists
    lateinit var mProductAdapter: CategoryListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val bundle = arguments
        if (bundle != null) {
            mCategory = bundle.getSerializable("data") as CategoryLists
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_category, container, false)
        mBinding.lifecycleOwner = viewLifecycleOwner
        return mBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mViewmodel = ViewModelProviders.of(this).get(CategoryViewmodel::class.java)
        mBinding.viewModel = mViewmodel
        initView()
        livedataObserver()
    }

    private fun livedataObserver() {
        mViewmodel.mPdtPerCat.observe(activity!!, Observer {
            if (it.success.data.products.isNotEmpty()) {
                mProductAdapter = CategoryListAdapter(
                    it,
                    context,
                    this
                )
                rvSubProducts.adapter = mProductAdapter

            } else {
                Toast.makeText(
                    activity,
                    "No produccts available for the selecyted Category",
                    Toast.LENGTH_LONG
                ).show()
            }
        })
    }

    private fun initView() {
        mBinding.toolbar.title_text.text = mCategory.name
        mViewmodel.loadProductsPerCategory(mCategory.id.toString())
    }
    override fun iclicked(pos: Int, view: View, any: Any) {


        when (view.id) {
/*
            R.id.constraint -> {
                val fragment = CategoryDetailFragment()
                var bundle = Bundle()
                bundle.putSerializable("data", any as CategoryProductResponse)
                bundle.putInt("position", pos)
                fragment.arguments = bundle
                FragmentUtils.addFragment(
                    activity as AppCompatActivity,
                    fragment,
                    R.id.categoryContainer,
                    true
                )
            }
*/
        }
    }

}