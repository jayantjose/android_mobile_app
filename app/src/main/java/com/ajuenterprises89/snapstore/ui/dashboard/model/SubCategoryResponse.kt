package com.ajuenterprises89.snapstore.ui.dashboard.model

data class SubCategoryResponse(
    val `data`: List<SubCategoryListData>,
    val success: String
)

data class SubCategoryListData(
    val category: CategoryList,
    val category_id: Int,
    val id: Int,
    val name: String,
    val name_ar: String,
    val slug: String,
    val status: Int
)

data class CategoryList(
    val id: Int,
    val image: String,
    val is_featured: Any,
    val name: String,
    val name_ar: String,
    val photo: String,
    val slug: String,
    val status: Int
)