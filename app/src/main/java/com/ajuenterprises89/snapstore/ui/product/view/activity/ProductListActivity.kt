package com.ajuenterprises89.snapstore.ui.product.view.activity

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.ajuenterprises89.snapstore.R
import com.ajuenterprises89.snapstore.ui.product.view.fragment.CategoryViewAllFragment
import com.ajuenterprises89.snapstore.ui.product.view.fragment.NewArrivalViewAllFragment
import com.ajuenterprises89.snapstore.ui.product.view.fragment.ProductListFragment
import com.ajuenterprises89.snapstore.utils.FragmentUtils

class ProductListActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_list)
        initView()
    }
    private fun initView() {
        val clickType = intent.getStringExtra("clickType")
        Log.e("typeeeeee",clickType)
        if(clickType.equals("ProductList")){
            val fragment = ProductListFragment()
            FragmentUtils.addFragment(
                this,
                fragment,
                R.id.productContainer,
                false
            )
        }else if(clickType.equals("NewArrivals")){
            val fragment = NewArrivalViewAllFragment()
            FragmentUtils.addFragment(
                this,
                fragment,
                R.id.productContainer,
                false
            )
        }
        else{
            val fragment = CategoryViewAllFragment()
            FragmentUtils.addFragment(
                this,
                fragment,
                R.id.productContainer,
                false
            )

        }

    }

}