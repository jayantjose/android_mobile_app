package com.ajuenterprises89.snapstore.ui.product.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.ajuenterprises89.snapstore.base.BaseViewModel
import com.ajuenterprises89.snapstore.network.Repository
import com.ajuenterprises89.snapstore.ui.dashboard.model.CategoryListResponse
import com.ajuenterprises89.snapstore.utils.SingleLiveEvent
import kotlinx.coroutines.launch

class CategoryViewAllViewModel: BaseViewModel<Any>() {
    val isloading = SingleLiveEvent<Boolean>()
    var mCategoryList = MutableLiveData<CategoryListResponse>()
    fun getCategoryList() {
        viewModelScope.launch {
            try {
                isloading.postValue(true)
                val call = Repository.categoryListing()
                if (call != null) {
                    isloading.postValue(false)
                    call.apply {
                        takeIf { isSuccessful } ?: throw Exception("Network Error")
                        takeIf { body() != null } ?: throw Exception("Server Error")
                        mCategoryList.postValue(this.body())
                    }
                } else {
                    isloading.postValue(false)
                    throw Exception("No Internet")
                }
            } catch (e: Exception) {
                isloading.postValue(false)
                Log.e("Exception", e.localizedMessage)
            }
        }

    }
}