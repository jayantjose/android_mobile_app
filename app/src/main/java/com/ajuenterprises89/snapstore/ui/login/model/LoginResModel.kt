package com.ajuenterprises89.snapstore.ui.login.model


data class LoginResponse(
    val `data`: Data,
    val message: String,
    val success: String
)

data class Data(
    val userid: Int,
    val username: String
)