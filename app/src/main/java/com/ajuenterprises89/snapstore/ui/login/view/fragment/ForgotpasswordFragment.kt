package com.ajuenterprises89.snapstore.ui.login.view.fragment

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.ajuenterprises89.snapstore.R
import com.ajuenterprises89.snapstore.databinding.FragmentForgotpasswordBinding
import com.ajuenterprises89.snapstore.ui.login.viewmodel.ForgotpasswordViewModel
import com.example.moeidbannerlibrary.banner.BaseBannerAdapter
import kotlinx.android.synthetic.main.fragment_forgotpassword.*

class ForgotpasswordFragment : Fragment() {
    lateinit var mBinding: FragmentForgotpasswordBinding


    private lateinit var viewModel: ForgotpasswordViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_forgotpassword, container, false)
        return  mBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ForgotpasswordViewModel::class.java)
        loadBanner()
        // TODO: Use the ViewModel
    }
    private fun loadBanner() {
        val urls: MutableList<String> = ArrayList()
        urls.add("https://homepages.cae.wisc.edu/~ece533/images/airplane.png")
        urls.add("https://homepages.cae.wisc.edu/~ece533/images/arctichare.png")
        urls.add("https://homepages.cae.wisc.edu/~ece533/images/arctichare.png")
        urls.add("https://homepages.cae.wisc.edu/~ece533/images/arctichare.png")
        urls.add("https://homepages.cae.wisc.edu/~ece533/images/arctichare.png")



        val webBannerAdapter = BaseBannerAdapter(activity, urls)
        webBannerAdapter.setOnBannerItemClickListener { }
        imageSlidersignFg.setAdapter(webBannerAdapter)    }

}