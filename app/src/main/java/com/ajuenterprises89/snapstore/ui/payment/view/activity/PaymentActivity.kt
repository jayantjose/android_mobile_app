package com.ajuenterprises89.snapstore.ui.payment.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ajuenterprises89.snapstore.R
import com.ajuenterprises89.snapstore.ui.payment.view.PaymentFragment
import com.ajuenterprises89.snapstore.utils.FragmentUtils

class PaymentActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)
        navigatePayment()
    }

    private fun navigatePayment() {
        FragmentUtils.addFragment(
            this,
            PaymentFragment(),
            R.id.paymentContainer,
            false
        )

    }
}