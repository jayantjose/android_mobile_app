package com.ajuenterprises89.snapstore.ui.product.view.fragment

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.ajuenterprises89.snapstore.R
import com.ajuenterprises89.snapstore.databinding.CategoryViewAllFragmentBinding
import com.ajuenterprises89.snapstore.databinding.NewArrivalViewAllFragmentBinding
import com.ajuenterprises89.snapstore.ui.dashboard.view.adapter.NewArrivalAdapter
import com.ajuenterprises89.snapstore.ui.product.view.adapter.NewArrivalListingAdapter
import com.ajuenterprises89.snapstore.ui.product.viewmodel.NewArrivalViewAllViewModel
import kotlinx.android.synthetic.main.dashboard_main.*
import kotlinx.android.synthetic.main.new_arrival_view_all_fragment.*
import kotlinx.android.synthetic.main.toolbar.view.*

class NewArrivalViewAllFragment : Fragment() {

    lateinit var mBinding: NewArrivalViewAllFragmentBinding
    private lateinit var viewModel: NewArrivalViewAllViewModel
    lateinit var mNewArrivalAdapter:NewArrivalListingAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.new_arrival_view_all_fragment, container, false)
        mBinding.lifecycleOwner = viewLifecycleOwner
        return mBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(NewArrivalViewAllViewModel::class.java)
        mBinding.viewModel = viewModel
        initView()
        viewModel.latestProducts()
        livedateObserver()
    }

    private fun livedateObserver() {
        viewModel.mLatestProductList.observe(activity!!, Observer {
            if (it.success.data.products.isNotEmpty()) {
                Log.e("adappppp","jdgjkdsgkdsf")
                mNewArrivalAdapter = NewArrivalListingAdapter(it.success.data.products,context!!,it.success.data.imagePath)
                rvproducts.adapter = mNewArrivalAdapter

            }
        })

    }
    private fun initView() {
        mBinding.toolbar.title_text.text = "New Arrival List"
    }

}