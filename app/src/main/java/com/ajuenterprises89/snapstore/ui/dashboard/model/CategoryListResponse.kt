package com.ajuenterprises89.snapstore.ui.dashboard.model

import java.io.Serializable

data class CategoryListResponse(
    val isSuccess: Boolean,
    val success: Success
)

data class Success(
    val `data`: Data,
    val message: String
)

data class Data(
    val categories: List<CategoryLists>,
    val imagePath: String
)

data class CategoryLists(
    val id: Int,
    val image: String,
    val is_featured: Int,
    val name: String,
    val name_ar: Any,
    val photo: String,
    val slug: String,
    val status: Int
):Serializable