package com.ajuenterprises89.snapstore.ui.product.models

data class ProductListResponse(
    val isSuccess: Boolean,
    val success: Success
)

data class Success(
    val `data`: Data,
    val message: String
)

data class Data(
    val imagePath: String,
    val products: List<Product>
)

data class Product(
    val affiliate_link: Any,
    val attributes: Any,
    val best: Int,
    val big: Int,
    val catalog_id: Int,
    val category: Category,
    val category_id: Int,
    val childcategory_id: Any,
    val color: Any,
    val colors: Any,
    val created_at: String,
    val details: String,
    val details_ar: Any,
    val discount_date: Any,
    val featured: Int,
    val features: Any,
    val `file`: String,
    val hot: Int,
    val id: Int,
    val is_catalog: Int,
    val is_discount: Int,
    val is_meta: Int,
    val latest: Int,
    val licence_type: Any,
    val license: Any,
    val license_qty: Any,
    val link: Any,
    val measure: Any,
    val meta_description: String,
    val meta_description_ar: Any,
    val meta_tag: Any,
    val meta_tag_ar: Any,
    val name: String,
    val name_ar: Any,
    val photo: String,
    val platform: Any,
    val policy: String,
    val policy_ar: Any,
    val previous_price: Int,
    val price: Int,
    val product_condition: Int,
    val product_type: String,
    val region: Any,
    val sale: Int,
    val ship: Any,
    val size: Any,
    val size_price: Any,
    val size_qty: Any,
    val sku: Any,
    val slug: String,
    val status: Int,
    val stock: Int,
    val subcategory: Subcategory,
    val subcategory_id: Any,
    val tags: Any,
    val thumbnail: String,
    val top: Int,
    val trending: Int,
    val type: String,
    val updated_at: String,
    val user_id: Int,
    val views: Int,
    val whole_sell_discount: Any,
    val whole_sell_qty: Any,
    val youtube: String
)

data class Category(
    val id: Int,
    val image: String,
    val is_featured: Int,
    val name: String,
    val name_ar: Any,
    val photo: String,
    val slug: String,
    val status: Int
)

data class Subcategory(
    val category_id: Any,
    val id: Int,
    val name: String,
    val name_ar: String,
    val slug: String,
    val status: Int
)