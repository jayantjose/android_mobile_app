package com.ajuenterprises89.snapstore.ui.cart

import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.ajuenterprises89.snapstore.R
import com.ajuenterprises89.snapstore.databinding.CartFragmentBinding
import com.ajuenterprises89.snapstore.databinding.ProfileFragmentBinding
import com.ajuenterprises89.snapstore.ui.dashboard.view.activity.DashboardActivity
import kotlinx.android.synthetic.main.common_toolbar.*

class CartFragment : Fragment() {
    lateinit var mBinding: CartFragmentBinding


    private lateinit var viewModel: CartViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
       // return inflater.inflate(R.layout.cart_fragment, container, false)
        mBinding = DataBindingUtil.inflate(inflater, R.layout.cart_fragment, container, false)
        mBinding.lifecycleOwner = viewLifecycleOwner
        return mBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(CartViewModel::class.java)
        toolbarClick()
    }
    private fun toolbarClick() {
        tvName.text="CART"
        ivDrawerBack.setOnClickListener {
            startActivity(Intent(context, DashboardActivity::class.java))
            activity!!.finish()
        }
    }

}