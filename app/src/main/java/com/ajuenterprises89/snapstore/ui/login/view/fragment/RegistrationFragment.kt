package com.ajuenterprises89.snapstore.ui.login.view.fragment

import android.app.ActionBar
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.ajuenterprises89.snapstore.R
import com.ajuenterprises89.snapstore.base.BaseFragment
import com.ajuenterprises89.snapstore.databinding.FragmentRegistrationBinding
import com.ajuenterprises89.snapstore.ui.dashboard.view.activity.DashboardActivity
import com.ajuenterprises89.snapstore.ui.login.viewmodel.RegistrationViewModel
import com.ajuenterprises89.snapstore.utils.FragmentUtils
import com.ajuenterprises89.snapstore.utils.Utils
import com.example.moeidbannerlibrary.banner.BaseBannerAdapter
import kotlinx.android.synthetic.main.fragment_registration.*

class RegistrationFragment : BaseFragment(),View.OnClickListener{

    lateinit var mBinding: FragmentRegistrationBinding
    private lateinit var mViewModel: RegistrationViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_registration, container, false)
        return  mBinding.root
       // return inflater.inflate(R.layout.fragment_registration, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mViewModel = ViewModelProvider(this).get(RegistrationViewModel::class.java)
        mBinding.viewModel=mViewModel
        mBinding.lifecycleOwner=viewLifecycleOwner
        setListner()
        loadBanner()
        apiObserver()
        errorMessageObserver()
        mBinding.viewModel = mViewModel
    }

    private fun apiObserver() {
        mViewModel.mRegResponse.observe(activity!!, Observer {
          navigateToDashboard()
        })
    }

    private fun errorMessageObserver() {
        mViewModel.mErrorMessage.observe(activity!!, Observer{
                showDialogMessage(it)
          //  Toast.makeText(activity!!,it,Toast.LENGTH_LONG).show()
        })
    }

    private fun setListner() {
        btnSignUp.setOnClickListener(this)
        tvSignUp_reg.setOnClickListener(this)

    }

    private fun loadBanner() {
        val urls: MutableList<String> = ArrayList()
        urls.add("https://homepages.cae.wisc.edu/~ece533/images/airplane.png")
        urls.add("https://homepages.cae.wisc.edu/~ece533/images/arctichare.png")
        urls.add("https://homepages.cae.wisc.edu/~ece533/images/arctichare.png")
        urls.add("https://homepages.cae.wisc.edu/~ece533/images/arctichare.png")
        urls.add("https://homepages.cae.wisc.edu/~ece533/images/arctichare.png")



        val webBannerAdapter = BaseBannerAdapter(activity, urls)
        webBannerAdapter.setOnBannerItemClickListener { }
        imageSlider.setAdapter(webBannerAdapter)    }

    override fun onClick(p0: View?) {
        when(p0!!.id){
            R.id.btnSignUp->{
                if(etUsername.text.toString().equals("")){
                  showDialogMessage("Enter email")
                }else if(!Utils.isEmailValid(etUsername.text.toString())){
                    showDialogMessage("Enter valid email")
                }else if(etPass.text.toString().equals("")){
                    showDialogMessage("Enter password")
                }else
                mViewModel.registration(etUsername.text.toString(),etPass.text.toString())

            }
            R.id.tvSignUp_reg->{
                FragmentUtils.replaceFragment(
                    activity as AppCompatActivity,
                    SignInFragment(),
                    R.id.fragmentContainer,
                    false
                )

            }
        }


    }
    fun showDialogMessage(message:String){
        var dialog = Dialog(activity!!, android.R.style.Theme_Dialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.alert_error_msg)
        dialog.setCanceledOnTouchOutside(false)
        dialog.getWindow()!!.setBackgroundDrawable( ColorDrawable(Color.TRANSPARENT));

        val tvMsg = dialog.findViewById(R.id.tvMsg) as com.ajuenterprises89.snapstore.custom.CustomTextViewBold
        val tvCross = dialog.findViewById(R.id.tvCross)  as com.ajuenterprises89.snapstore.custom.CustomTextViewBold
        tvMsg.text=message
        tvCross.setOnClickListener {
            dialog.dismiss()
        }

        dialog.setCanceledOnTouchOutside(false)
        dialog.getWindow()!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        dialog.getWindow()!!.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT)
        dialog.show()

    }

    private fun navigateToDashboard() {
        var dialog = Dialog(activity!!, android.R.style.Theme_Dialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.alert_success_msg)
        dialog.setCanceledOnTouchOutside(false)
        dialog.getWindow()!!.setBackgroundDrawable( ColorDrawable(Color.TRANSPARENT));

        val tvMsg = dialog.findViewById(R.id.tvMsg) as com.ajuenterprises89.snapstore.custom.CustomTextViewBold
        val tvCross = dialog.findViewById(R.id.tvCross)  as com.ajuenterprises89.snapstore.custom.CustomTextViewBold
        tvMsg.text="Registration Success"
        tvCross.setOnClickListener {
            startActivity(Intent(activity, DashboardActivity::class.java))
            activity!!.finish()
            dialog.dismiss()
        }

        dialog.setCanceledOnTouchOutside(false)
        dialog.getWindow()!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        dialog.getWindow()!!.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT)
        dialog.show()
    }

}