package com.ajuenterprises89.snapstore.ui.product.view.fragment

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.ajuenterprises89.snapstore.R
import com.ajuenterprises89.snapstore.databinding.CategoryViewAllFragmentBinding
import com.ajuenterprises89.snapstore.ui.product.view.adapter.CompleteCategoryListAdapter
import com.ajuenterprises89.snapstore.ui.product.view.adapter.CompleteProductListingAdapter
import com.ajuenterprises89.snapstore.ui.product.viewmodel.CategoryViewAllViewModel
import kotlinx.android.synthetic.main.category_view_all_fragment.*
import kotlinx.android.synthetic.main.toolbar.view.*

class CategoryViewAllFragment : Fragment() {

    private lateinit var viewModel: CategoryViewAllViewModel
    lateinit var mBinding: CategoryViewAllFragmentBinding
    lateinit var completeCategoryListAdapter: CompleteCategoryListAdapter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.category_view_all_fragment, container, false)
        mBinding.lifecycleOwner = viewLifecycleOwner
        return mBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(CategoryViewAllViewModel::class.java)
        mBinding.viewModel = viewModel
        initView()
        viewModel.getCategoryList()
        loveDataObserver()
    }
    private fun loveDataObserver() {
        viewModel.mCategoryList.observe(activity!!, Observer {
            if (it.success.data.categories.isNotEmpty()) {
                completeCategoryListAdapter =
                    CompleteCategoryListAdapter(it.success.data.categories, context!!,it.success.data.imagePath)
                rvproducts.adapter = completeCategoryListAdapter

            }
        })
    }
    private fun initView() {
        mBinding.toolbar.title_text.text = "Category List"
    }
}