package com.ajuenterprises89.snapstore.ui.settings.view

import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.ajuenterprises89.snapstore.R
import com.ajuenterprises89.snapstore.databinding.CartFragmentBinding
import com.ajuenterprises89.snapstore.databinding.SettingsFragmentBinding
import com.ajuenterprises89.snapstore.ui.dashboard.view.activity.DashboardActivity
import com.ajuenterprises89.snapstore.ui.settings.viewmodel.SettingsViewModel
import kotlinx.android.synthetic.main.common_toolbar.*

class SettingsFragment : Fragment() {
    lateinit var mBinding: SettingsFragmentBinding


    private lateinit var viewModel: SettingsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //return inflater.inflate(R.layout.settings_fragment, container, false)
        mBinding = DataBindingUtil.inflate(inflater, R.layout.settings_fragment, container, false)
        mBinding.lifecycleOwner = viewLifecycleOwner
        return mBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(SettingsViewModel::class.java)
        toolbarClick()
    }
    private fun toolbarClick() {
        tvName.text="SETTINGS"
        ivDrawerBack.setOnClickListener {
            startActivity(Intent(context, DashboardActivity::class.java))
            activity!!.finish()
        }
    }

}