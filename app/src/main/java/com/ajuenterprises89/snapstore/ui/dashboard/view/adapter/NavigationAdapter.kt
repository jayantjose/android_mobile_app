package com.ajuenterprises89.snapstore.ui.dashboard.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ajuenterprises89.snapstore.R
import com.ajuenterprises89.snapstore.interfaces.RvItemClicked
import com.ajuenterprises89.snapstore.ui.dashboard.model.NavigationMenuItem
import kotlinx.android.synthetic.main.navigation_view.view.*

class NavigationAdapter(
    val data: List<NavigationMenuItem>,
    val context: Context?, val itemClicked: RvItemClicked
) :
    RecyclerView.Adapter<NavigationAdapter.ViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): NavigationAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.navigation_view, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: NavigationAdapter.ViewHolder, position: Int) {
        val data = data[position]

        holder.itemView.tvTitle.text = data.title
        holder.itemView.tvSubTitle.text = data.subTitle
        holder.itemView.imgIconType.setBackgroundResource(data.icon);
        holder.itemView.constraint.setOnClickListener {
            itemClicked.iclicked(position, it, data)
        }

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }
}