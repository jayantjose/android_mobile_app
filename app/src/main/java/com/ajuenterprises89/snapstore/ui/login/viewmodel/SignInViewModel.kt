package com.ajuenterprises89.snapstore.ui.login.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.ajuenterprises89.snapstore.base.BaseViewModel
import com.ajuenterprises89.snapstore.network.Repository
import com.ajuenterprises89.snapstore.ui.login.model.LoginReqModel
import com.ajuenterprises89.snapstore.ui.login.model.LoginResponse
import com.ajuenterprises89.snapstore.utils.SingleLiveEvent
import com.ajuenterprises89.snapstore.utils.Utils.isEmailValid
import kotlinx.coroutines.launch

class SignInViewModel : BaseViewModel<Any>() {
    //    var mDocumentType = MutableLiveData<List<DocumentTypeMaster>>()
    val isloading = SingleLiveEvent<Boolean>()
    var mMessageObserver=MutableLiveData<String>()
    var mLoginResponse=MutableLiveData<LoginResponse>()


    fun doLogin(username: String, password: String) {

        viewModelScope.launch {
            try {
              isloading.postValue(true)
                val call = Repository.login(username,password)
                if (call != null) {
                  isloading.postValue(false)
                    call.apply {
                        takeIf { isSuccessful } ?: throw Exception("Network Error")
                        takeIf { body() != null } ?: throw Exception("Server Error")
                        //  takeIf { body()?.CommonEntity?.IsAuthourized == "Y" } ?: throw Exception(body()?.CommonEntity?.Message)
//                      mDocumentType.postValue(body()?.MasterList)
                        mLoginResponse.postValue(this.body())
                    }
                } else {
                    isloading.postValue(false)

                    throw Exception("No Internet")
                }
            } catch (e: Exception) {
                isloading.postValue(false)

                Log.e("Exception", e.localizedMessage)
//                showdialog.postValue(e.localizedMessage)
//              isloading.postValue(false)
            }
        }

    }
}