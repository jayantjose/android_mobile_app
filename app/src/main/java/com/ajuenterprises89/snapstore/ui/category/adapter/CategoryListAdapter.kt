package com.ajuenterprises89.snapstore.ui.category.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ajuenterprises89.snapstore.R
import com.ajuenterprises89.snapstore.base.Constants
import com.ajuenterprises89.snapstore.interfaces.RvItemClicked
import com.ajuenterprises89.snapstore.ui.category.models.CategoryProductResponse
import com.ajuenterprises89.snapstore.ui.dashboard.model.Product
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.category_main_view.view.*

class CategoryListAdapter(
    val mdata: CategoryProductResponse,
    val context: Context?,
    val rvItemClicked: RvItemClicked
) :
    RecyclerView.Adapter<CategoryListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v =
            LayoutInflater.from(parent.context).inflate(R.layout.category_main_view, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return mdata.success.data.products.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data =  mdata.success.data.products[position]
        holder.itemView.tvProductName.text = data.name
        holder.itemView.tvSellerName.text = data.sku
        holder.itemView.tvPrice.text = data.size_price.toString()
        Picasso.with(context).load(mdata.success.data.imagePath + data.photo)
            .into(holder.itemView.iv_imageSlider)
        holder.itemView.constraint.setOnClickListener{
            rvItemClicked.iclicked(position,it, mdata)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }
}
