package com.ajuenterprises89.snapstore.ui.splash

import android.R.attr.top
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.ajuenterprises89.snapstore.R
import com.ajuenterprises89.snapstore.base.BaseActivity
import com.ajuenterprises89.snapstore.ui.dashboard.view.activity.DashboardActivity
import com.ajuenterprises89.snapstore.ui.login.view.activity.LoginActivity
import kotlinx.android.synthetic.main.activity_splash.*


class SplashActivity : BaseActivity() {
    private val SPLASH_TIME_OUT: Long = 4000
    private val TEXT_TIME_OUT: Long = 3500

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        animateTextField()
        loginNavigation()

    }

    private fun loginNavigation() {
        Handler().postDelayed({
//            startActivity(Intent(this, LoginActivity::class.java))
            startActivity(Intent(this, DashboardActivity::class.java))
            finish()
        }, SPLASH_TIME_OUT)
    }

    private fun animateTextField() {
        Handler().postDelayed({
            tvSnapStore.animate().x(0f).y(100f);
        }, TEXT_TIME_OUT)

    }
}
