package com.ajuenterprises89.snapstore.ui.dashboard.view.fragment

import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.ajuenterprises89.snapstore.R
import com.ajuenterprises89.snapstore.base.BaseFragment
import com.ajuenterprises89.snapstore.databinding.FragmentDashboardBinding
import com.ajuenterprises89.snapstore.interfaces.RvItemClicked
import com.ajuenterprises89.snapstore.ui.cart.CartFragment
import com.ajuenterprises89.snapstore.ui.category.view.activity.CategoryActivity
import com.ajuenterprises89.snapstore.ui.dashboard.view.adapter.CategoryListingAdapter
import com.ajuenterprises89.snapstore.ui.dashboard.view.adapter.NavigationAdapter
import com.ajuenterprises89.snapstore.ui.dashboard.view.adapter.ProductListingAdapter
import com.ajuenterprises89.snapstore.ui.dashboard.model.CategoryLists
import com.ajuenterprises89.snapstore.ui.dashboard.model.NavigationMenuItem
import com.ajuenterprises89.snapstore.ui.dashboard.model.Slider
import com.ajuenterprises89.snapstore.ui.dashboard.view.activity.DashboardActivity
import com.ajuenterprises89.snapstore.ui.dashboard.view.adapter.*
import com.ajuenterprises89.snapstore.ui.dashboard.viewmodel.DashboardViewmodel
import com.ajuenterprises89.snapstore.ui.payment.view.PaymentFragment
import com.ajuenterprises89.snapstore.ui.payment.view.activity.PaymentActivity
import com.ajuenterprises89.snapstore.ui.product.view.activity.ProductListActivity
import com.ajuenterprises89.snapstore.utils.FragmentUtils
import kotlinx.android.synthetic.main.app_bar_dashboard.view.*
import kotlinx.android.synthetic.main.dashboard_main.*
import kotlinx.android.synthetic.main.dashboard_main.view.*
import kotlinx.android.synthetic.main.dashboard_toolbar.view.*
import kotlinx.android.synthetic.main.fragment_dashboard.*
import liang.lollipop.rvbannerlib.BannerUtil
import liang.lollipop.rvbannerlib.BannerUtil4J
import liang.lollipop.rvbannerlib.banner.Orientation


class DashboardFragment : BaseFragment(), View.OnClickListener, RvItemClicked {


    lateinit var mBinding: FragmentDashboardBinding
    lateinit var mViewmodel: DashboardViewmodel
    lateinit var drawer: DrawerLayout
    lateinit var categoryListingAdapter: CategoryListingAdapter
    lateinit var mProductAdapter: ProductListingAdapter
    lateinit var mNewArrivalAdapter: NewArrivalAdapter
    lateinit var mNavigationAdapter: NavigationAdapter
    private var bannerUtil4J: BannerUtil4J? = null
    lateinit var bannerListAdapter: BannerListAdapter
    lateinit var categoryList: List<CategoryLists>

    var mNavItems = arrayListOf<NavigationMenuItem>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_dashboard, container, false)
        mBinding.lifecycleOwner = viewLifecycleOwner
        return mBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mViewmodel = ViewModelProviders.of(this).get(DashboardViewmodel::class.java)
        clickListner()
        //  mBinding.viewmodel = mViewmodel
        initView()
        mViewmodel.getCategoryList()
        mViewmodel.getProductList()
        //mViewmodel.getSubcateoryList()
        mViewmodel.loadBannerList()
        mViewmodel.latestProducts()
        liveaDataObserver()
    }

    private fun clickListner() {
        constraintCategory.setOnClickListener(this)
        constraintCart.setOnClickListener(this)
        constraintPayment.setOnClickListener(this)
        constraintShareApp.setOnClickListener(this)
    }

    private fun liveaDataObserver() {
        mViewmodel.mCategoryList.observe(activity!!, Observer {
            if (it.success.data.categories.isNotEmpty()) {
                categoryList=it.success.data.categories
                categoryListingAdapter =
                    CategoryListingAdapter(it.success.data.categories, context!!, this)
                recyclerCategory.adapter = categoryListingAdapter

            }
        })

        mViewmodel.mLatestProductList.observe(activity!!, Observer {
            if (it.success.data.products.isNotEmpty()) {
                mNewArrivalAdapter = NewArrivalAdapter(it.success.data.products,it.success.data.imagePath, context)
                recyclerNewArrivals.adapter = mNewArrivalAdapter

            }
        })


        mViewmodel.mProductList.observe(activity!!, Observer {
            if (it.success.data.products.isNotEmpty()) {
                mProductAdapter = ProductListingAdapter(it.success.data.products, context)
                recyclerBestSelling.adapter = mProductAdapter

            }
        })

        /////
        mViewmodel.mBannerList.observe(activity!!, Observer {
            if (it.success.data.sliders.isNotEmpty()) {

                bannerListAdapter = BannerListAdapter(
                    it.success.data.sliders as ArrayList<Slider>, activity!!,
                    layoutInflater, it.success.data.imagePath
                )
                BannerUtil.with(recyclerSlider)
                    .attachAdapter(bannerListAdapter)
                    .setOrientation(Orientation.HORIZONTAL)
                    .setSecondaryExposedWeight(0.15F)
                    .init()
                    .isAutoNext(auto = true)

                recyclerSlider.adapter!!.notifyDataSetChanged()
            }
        })
    }


    private fun initView() {

        mBinding.appBar.frament_dash.toolbar.ivDrawer.setOnClickListener(this)
        tvBestSellingViewAll.setOnClickListener(this)
        tvCategoryViewAll.setOnClickListener(this)
        tvNewArrivalsViewAll.setOnClickListener(this)
        ivCategory.setOnClickListener(this)
        ivPayment.setOnClickListener(this)
        val toolbar = mBinding.appBar.frament_dash.toolbars
        drawer = mBinding.drawerLayout
        val navigationView = mBinding.navView
        navigationView.itemIconTintList = null
        val toggle = ActionBarDrawerToggle(
            activity,
            drawer,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer.addDrawerListener(toggle)
        toggle.syncState()
        var recyclerBestSelling = recyclerBestSelling
        var layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        recyclerBestSelling.layoutManager = layoutManager
    }




    override fun iclicked(pos: Int, view: View, any: Any) {

        when (view.id) {

            R.id.mainCardView -> {
                var bundle = Bundle()
                var intent = Intent(context, CategoryActivity::class.java)
                bundle.putSerializable("category", any as CategoryLists)
                intent.putExtras(bundle)
                startActivity(intent)
            }

        }

        drawer.closeDrawer(Gravity.LEFT)

    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivDrawer -> {
                val drawer = mBinding.drawerLayout
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START)
                } else {
                    drawer.openDrawer(GravityCompat.START)
                }
            }
            R.id.constraintCategory -> {

            }
            R.id.constraintCart -> {
                FragmentUtils.replaceFragment(
                    activity  as AppCompatActivity,
                    CartFragment(),
                    R.id.dashboardContainer,
                    false
                )
            }
            R.id.constraintPayment -> {
                startActivity(Intent(context, PaymentActivity::class.java))
            }
            R.id.constraintShareApp -> {

            }
             R.id.tvBestSellingViewAll -> {
                var intent = Intent(context, ProductListActivity::class.java)
                intent.putExtra("clickType","ProductList")
                startActivity(intent)
            }
            R.id.tvCategoryViewAll->{
                var intent = Intent(context, ProductListActivity::class.java)
                intent.putExtra("clickType","CategoryList")
                startActivity(intent)
            }

            R.id.tvNewArrivalsViewAll->{
                var intent = Intent(context, ProductListActivity::class.java)
                intent.putExtra("clickType","NewArrivals")
                startActivity(intent)
            }
            R.id.ivCategory->{
                Log.e("gdsgdsgds","dsgdsgdsgsd")
            }
            R.id.ivPayment->{
                var intent = Intent(context, PaymentActivity::class.java)
                startActivity(intent)
            }
        }

    }

}
