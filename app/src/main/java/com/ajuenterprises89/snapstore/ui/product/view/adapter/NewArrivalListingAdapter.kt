package com.ajuenterprises89.snapstore.ui.product.view.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.ajuenterprises89.snapstore.R
import com.ajuenterprises89.snapstore.ui.dashboard.model.LatestProductsDataProduct
import com.ajuenterprises89.snapstore.ui.product.models.Product
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.new_arrival_item_view_all.view.*

class NewArrivalListingAdapter(
    var mCategoryList: List<LatestProductsDataProduct>,
    var context: Context,
    var path: String
) : BaseAdapter(){
    private var layoutInflater: LayoutInflater? = null

    override fun getItem(position: Int): Any {
        return mCategoryList.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return mCategoryList.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var convertView = convertView
        if (layoutInflater == null) {
            layoutInflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }
        if (convertView == null) {
            convertView = layoutInflater!!.inflate(R.layout.new_arrival_item_view_all, null)
        }
        Picasso.with(context).load(path+ mCategoryList[position].photo).into(convertView!!.iv_imageSlider)
        convertView!!.tvProductName.text = mCategoryList[position].name
        convertView!!.tvSellerName.text = mCategoryList[position].status.toString()
        convertView!!.tvAmount.text ="$"+ mCategoryList[position].previous_price.toString()
        return convertView
    }

}