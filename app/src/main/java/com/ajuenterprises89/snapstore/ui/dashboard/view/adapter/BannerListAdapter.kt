package com.ajuenterprises89.snapstore.ui.dashboard.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.ajuenterprises89.snapstore.R
import com.ajuenterprises89.snapstore.base.Constants
import com.ajuenterprises89.snapstore.ui.dashboard.model.BannerList
import com.ajuenterprises89.snapstore.ui.dashboard.model.Product
import com.ajuenterprises89.snapstore.ui.dashboard.model.Slider
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.card_imagelist.view.*
import kotlinx.android.synthetic.main.card_imagelist.view.iv_imageSlider
import kotlinx.android.synthetic.main.product_item_view.view.*
import java.util.*
import kotlin.collections.ArrayList

/*@Override
public boolean onNavigationItemSelected(@NonNull MenuItem item) {
   return false;
}*/
 class BannerListAdapter(
    data: ArrayList<Slider>,
    val context: Context?,
    layoutInflater: LayoutInflater,val imagePath:String
) :
    RecyclerView.Adapter<BannerListAdapter.TestHolder>() {
    private val data: ArrayList<Slider>
    private val layoutInflater: LayoutInflater
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BannerListAdapter.TestHolder {
        return BannerListAdapter.TestHolder(
            layoutInflater.inflate(
                R.layout.card_banner,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: TestHolder, position: Int) {
        //holder.onBind(data[position])
        Picasso.with(context).load(imagePath +data.get(position).photo)
            .into(holder.itemView.iv_imageSlider)

    }

    init {
        this.data = data
        this.layoutInflater = layoutInflater
    }

    class TestHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        private val textView: AppCompatImageView
        fun onBind(bean: Product) {
            //onBind(bean.text, bean.color)
        }


        init {
            textView = itemView.findViewById(R.id.iv_imageSlider)
        }
    }

}
