package com.ajuenterprises89.snapstore.ui.category.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.ajuenterprises89.snapstore.R
import com.ajuenterprises89.snapstore.base.BaseFragment
import com.ajuenterprises89.snapstore.databinding.FragmentCategoryDetailBinding
import com.ajuenterprises89.snapstore.ui.category.models.CategoryProductResponse
import com.ajuenterprises89.snapstore.ui.category.viewmodel.CategoryDetailViewmodel
import com.squareup.picasso.Picasso

class CategoryDetailFragment : BaseFragment() {
    lateinit var mBinding: FragmentCategoryDetailBinding
    lateinit var mViewmodel: CategoryDetailViewmodel
    lateinit var mProduct: CategoryProductResponse
     var position=0



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val bundle = arguments
        if (bundle != null) {
            mProduct = bundle.getSerializable("data") as CategoryProductResponse
            position = bundle.getInt("position")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_category_detail, container, false)
        mBinding.lifecycleOwner = viewLifecycleOwner
        return mBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mViewmodel = ViewModelProviders.of(this).get(CategoryDetailViewmodel::class.java)
        mBinding.mViewmodel = mViewmodel
        initView()
        livedataObserver()
    }

    private fun livedataObserver() {
        Picasso.with(context).load(mProduct.success.data.imagePath + mProduct.success.data.products[position].photo)
            .into(mBinding.ivImageSlider)
    }

    private fun initView() {

    }
}