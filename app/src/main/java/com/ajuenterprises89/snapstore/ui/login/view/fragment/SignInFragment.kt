package com.ajuenterprises89.snapstore.ui.login.view.fragment

import android.app.ActionBar
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ajuenterprises89.snapstore.R
import com.ajuenterprises89.snapstore.base.BaseFragment
import com.ajuenterprises89.snapstore.databinding.FragmentSignInBinding
import com.ajuenterprises89.snapstore.ui.dashboard.view.activity.DashboardActivity
import com.ajuenterprises89.snapstore.ui.login.viewmodel.SignInViewModel
import com.ajuenterprises89.snapstore.utils.FragmentUtils
import com.ajuenterprises89.snapstore.utils.Utils
import com.example.moeidbannerlibrary.banner.BaseBannerAdapter
import kotlinx.android.synthetic.main.fragment_sign_in.*


class SignInFragment : BaseFragment() ,View.OnClickListener {
    lateinit var mBinding: FragmentSignInBinding
    lateinit var mViewmodel: SignInViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_in, container, false)
        return mBinding.root
    }

    private fun loadBanner() {
        val urls: MutableList<String> = ArrayList()
        urls.add("https://homepages.cae.wisc.edu/~ece533/images/airplane.png")
        urls.add("https://homepages.cae.wisc.edu/~ece533/images/arctichare.png")
        urls.add("https://homepages.cae.wisc.edu/~ece533/images/arctichare.png")
        urls.add("https://homepages.cae.wisc.edu/~ece533/images/arctichare.png")
        urls.add("https://homepages.cae.wisc.edu/~ece533/images/arctichare.png")


        val webBannerAdapter = BaseBannerAdapter(activity, urls)
        webBannerAdapter.setOnBannerItemClickListener { }
        imageSlidersignup.setAdapter(webBannerAdapter)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mViewmodel = ViewModelProviders.of(this).get(SignInViewModel::class.java)
        mBinding.lifecycleOwner=this
        mBinding.viewModel = mViewmodel
        onclicklistner()
        loadBanner()
        clickListner()
        errormessageObserver()
        loginSuccessObserver()
    }

    private fun loginSuccessObserver() {
        mViewmodel.mLoginResponse.observe(activity!!, Observer {
            if(it.success=="true"){
                navigateToDashboard()

            }else{
                showDialogMessage(it.message)

               // Toast.makeText(activity,it.message,Toast.LENGTH_LONG).show()
            }
        })
    }

    private fun navigateToDashboard() {
        var dialog = Dialog(activity!!, android.R.style.Theme_Dialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.alert_success_msg)
        dialog.setCanceledOnTouchOutside(false)
        dialog.getWindow()!!.setBackgroundDrawable( ColorDrawable(Color.TRANSPARENT));

        val tvMsg = dialog.findViewById(R.id.tvMsg) as com.ajuenterprises89.snapstore.custom.CustomTextViewBold
        val tvCross = dialog.findViewById(R.id.tvCross)  as com.ajuenterprises89.snapstore.custom.CustomTextViewBold
        tvMsg.text="Login Success"
        tvCross.setOnClickListener {
            startActivity(Intent(activity, DashboardActivity::class.java))
            activity!!.finish()
            dialog.dismiss()
        }

        dialog.setCanceledOnTouchOutside(false)
        dialog.getWindow()!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        dialog.getWindow()!!.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT)
        dialog.show()
    }

    private fun errormessageObserver() {
     mViewmodel.mMessageObserver.observe(activity!!, Observer {
         //Toast.makeText(activity,it,Toast.LENGTH_LONG).show()
         showDialogMessage(it)

     })
    }

    private fun clickListner() {
        mBinding.btnSignIn.setOnClickListener(this)
    }


    private fun onclicklistner() {
        tvSignUp.setOnClickListener(this)
        btnSignIn.setOnClickListener(this)
        tvFgPass.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.tvSignUp -> {
                FragmentUtils.addFragment(
                    activity as AppCompatActivity,
                    RegistrationFragment(),
                    R.id.fragmentContainer,
                    true
                )
            }
            R.id.btnSignIn -> {
                if(etUsername.text.toString().equals("")){
                    showDialogMessage("Enter email")
                }else if(!Utils.isEmailValid(etUsername.text.toString())){
                    showDialogMessage("Enter valid email")
                }
                else if(etPass.text.toString().equals("")){
                    showDialogMessage("Enter password")
                }else
                mViewmodel.doLogin(etUsername.text.toString(), etPass.text.toString())
            }
            R.id.btnSignIn -> startActivity(Intent(context, DashboardActivity::class.java))
            R.id.tvFgPass -> {
                FragmentUtils.addFragment(
                    activity as AppCompatActivity,
                    ForgotpasswordFragment(),
                    R.id.fragmentContainer,
                    true
                )
            }

        }

    }
    fun showDialogMessage(message:String){
        var dialog = Dialog(activity!!, android.R.style.Theme_Dialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.alert_error_msg)
        dialog.setCanceledOnTouchOutside(false)
        dialog.getWindow()!!.setBackgroundDrawable( ColorDrawable(Color.TRANSPARENT));

        val tvMsg = dialog.findViewById(R.id.tvMsg) as com.ajuenterprises89.snapstore.custom.CustomTextViewBold
        val tvCross = dialog.findViewById(R.id.tvCross)  as com.ajuenterprises89.snapstore.custom.CustomTextViewBold
        tvMsg.text=message
        tvCross.setOnClickListener {
            dialog.dismiss()
        }

        dialog.setCanceledOnTouchOutside(false)
        dialog.getWindow()!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        dialog.getWindow()!!.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT)
        dialog.show()

    }
}

