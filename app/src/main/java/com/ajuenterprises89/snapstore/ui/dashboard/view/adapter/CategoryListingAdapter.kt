package com.ajuenterprises89.snapstore.ui.dashboard.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.ajuenterprises89.snapstore.R
import com.ajuenterprises89.snapstore.base.Constants.CATEGORY_URL
import com.ajuenterprises89.snapstore.interfaces.RvItemClicked
import com.ajuenterprises89.snapstore.ui.dashboard.model.CategoryLists
import com.ajuenterprises89.snapstore.ui.dashboard.model.Data
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.card_imagelist.view.*
import java.lang.reflect.Array.get
import java.nio.file.Paths.get
import kotlin.math.acos

class CategoryListingAdapter(
    var mCategoryList: List<CategoryLists>,
    var context: Context,
    var rvItemClicked: RvItemClicked
) : RecyclerView.Adapter<CategoryListingAdapter.ViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CategoryListingAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.card_imagelist, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return 5
    }

    override fun onBindViewHolder(holder: CategoryListingAdapter.ViewHolder, position: Int) {
        Picasso.with(context).load(CATEGORY_URL + mCategoryList[position].image)
            .into(holder.iv_imageSlider)
        holder.tvCategoryName.text = mCategoryList[position].name
        holder.tvCount.text = mCategoryList[position].status.toString()
        holder.itemView.mainCardView.setOnClickListener {
            rvItemClicked.iclicked(position, it, mCategoryList[position])
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var iv_imageSlider = itemView.findViewById(R.id.iv_imageSlider) as AppCompatImageView
        var tvCategoryName =
            itemView.findViewById(R.id.tvCategoryName) as com.ajuenterprises89.snapstore.custom.CustomTextView
        var tvCount =
            itemView.findViewById(R.id.tvCount) as com.ajuenterprises89.snapstore.custom.CustomTextView
    }


}