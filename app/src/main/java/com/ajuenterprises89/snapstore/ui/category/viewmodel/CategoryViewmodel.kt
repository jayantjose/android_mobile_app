package com.ajuenterprises89.snapstore.ui.category.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.ajuenterprises89.snapstore.base.BaseViewModel
import com.ajuenterprises89.snapstore.network.Repository
import com.ajuenterprises89.snapstore.ui.category.models.CategoryProductResponse
import com.ajuenterprises89.snapstore.utils.SingleLiveEvent
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.RequestBody

class CategoryViewmodel : BaseViewModel<Any>(){
    val isloading = SingleLiveEvent<Boolean>()
    var mPdtPerCat= MutableLiveData<CategoryProductResponse>()
    fun loadProductsPerCategory(catId:String){
        viewModelScope.launch {
            isloading.postValue(true)
            //val catId=    RequestBody.create(MediaType.parse("text/plain"), catId)
            try {
                val call = Repository.loadProductsPerCategory(catId)
                if (call != null) {
                    isloading.postValue(false)
                    call.apply {
                        mPdtPerCat.postValue(this.body())

                        takeIf { isSuccessful } ?: throw Exception("Network Error")
                        takeIf { body() != null } ?: throw Exception("Server Error")
                    }
                } else {
                    isloading.postValue(false)
                    throw Exception("No Internet")
                }
            } catch (e: Exception) {
                isloading.postValue(false)
                Log.e("Exception", e.localizedMessage)
            }
        }

    }
}