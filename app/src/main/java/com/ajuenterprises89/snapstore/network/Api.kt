package com.ajuenterprises89.snapstore.network

import com.ajuenterprises89.snapstore.ui.category.models.CategoryProductResponse
import com.ajuenterprises89.snapstore.ui.dashboard.model.*
import com.ajuenterprises89.snapstore.ui.login.model.LoginResponse
import com.ajuenterprises89.snapstore.ui.login.model.RegistrationSuccess
import com.ajuenterprises89.snapstore.ui.product.models.ProductListResponse
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*


interface Api {
   @FormUrlEncoded
   @POST("login")
    suspend fun login(@Field("email") username: String, @Field("password") password:String): Response<LoginResponse>?



    @Multipart
    @POST("register")
    suspend fun registration(@Part("email") email: RequestBody, @Part("password") password:RequestBody
                             , @Part("name")name:RequestBody, @Part("phone")ph0ne:RequestBody, @Part("address")address:RequestBody,
                             @Part("password_confirmation")password_confirmation:RequestBody): Response<RegistrationSuccess>?


    @Headers("X-Authorization: DwNxDhNCfRif0wZawyfRHOKtYNnIwxy89B0FhzQ1zYjArPh0rzIfelpSyr7mcOq0")
    @GET("categories")
    suspend fun categoryListing():Response<CategoryListResponse>

    @Headers("X-Authorization: DwNxDhNCfRif0wZawyfRHOKtYNnIwxy89B0FhzQ1zYjArPh0rzIfelpSyr7mcOq0")
    @GET("bestsellers")
    suspend fun productListing():Response<ProductListingResponse>

    @Headers("X-Authorization: DwNxDhNCfRif0wZawyfRHOKtYNnIwxy89B0FhzQ1zYjArPh0rzIfelpSyr7mcOq0")
    @GET("subcategories")
    suspend fun subcategoryList():Response<SubCategoryResponse>

    @Headers("X-Authorization: DwNxDhNCfRif0wZawyfRHOKtYNnIwxy89B0FhzQ1zYjArPh0rzIfelpSyr7mcOq0")
    @GET("homepage/sliders")
    suspend fun bannerList():Response<BannerList>


    @Headers("X-Authorization: DwNxDhNCfRif0wZawyfRHOKtYNnIwxy89B0FhzQ1zYjArPh0rzIfelpSyr7mcOq0")
    @GET("latest-products")
    suspend fun latestProducts():Response<LatestProductsResponse>

   @Headers("X-Authorization: DwNxDhNCfRif0wZawyfRHOKtYNnIwxy89B0FhzQ1zYjArPh0rzIfelpSyr7mcOq0")
    //@GET("products?main_category=5")
    @GET("products")
 //  suspend fun productsPerCategory(@Path("main_category") catId: RequestBody):Response<CategoryProductResponse>
    suspend fun productsPerCategory(@Query("main_category")catId: String):Response<CategoryProductResponse>

 @Headers("X-Authorization: DwNxDhNCfRif0wZawyfRHOKtYNnIwxy89B0FhzQ1zYjArPh0rzIfelpSyr7mcOq0")
 @GET("products")
 suspend fun getAllProductsList():Response<ProductListResponse>

}

