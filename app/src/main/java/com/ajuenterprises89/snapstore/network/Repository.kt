package com.ajuenterprises89.snapstore.network

import okhttp3.RequestBody

object Repository {
    suspend fun login(username: String, password: String)=ApiClient.create().login(username = username, password = password)
   /* suspend fun registration(email: String, password: String)=ApiClient.create().registration(email = email, password = password,
    name="",ph0ne = "",address = "",password_confirmation = password)*/

    suspend fun registration(email: RequestBody, password: RequestBody,name:RequestBody,phone:RequestBody,address:RequestBody,password_confirmation:RequestBody)=ApiClient.create().registration(email = email, password = password,
        name=name,ph0ne = phone,address = address,password_confirmation = password_confirmation)
    //suspend fun registration()
    suspend fun categoryListing()=ApiClient.create().categoryListing()
    suspend fun productListing()=ApiClient.create().productListing()
    suspend fun subCategoryListing()=ApiClient.create().subcategoryList()
    suspend fun bannerList()=ApiClient.create().bannerList()
    suspend fun latestProducts()=ApiClient.create().latestProducts()
    suspend fun loadProductsPerCategory(catId: String)=ApiClient.create().productsPerCategory(catId=catId)
    suspend fun productListViewAll()=ApiClient.create().getAllProductsList()

 //   suspend fun loadProductsPerCategory(catId: RequestBody)=ApiClient.create().productsPerCategory()

}